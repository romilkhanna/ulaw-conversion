#include <stdio.h>

int main () {
	signed short sample;
	char comp;

	FILE *fp;
	fp = fopen("sample.wav", "rb");

	FILE *out;
	out = fopen("output.wav", "wb+");

	long lVal;
	short sVal;

	fread(&lVal, sizeof(lVal), 1, fp);
	fwrite(&lVal, sizeof(lVal), 1, out);

	fread(&lVal, sizeof(lVal), 1, fp);
	lVal = ((lVal-36) >> 1) + 38;
	fwrite(&lVal, sizeof(lVal), 1, out);

	fread(&lVal, sizeof(lVal), 1, fp);
	fwrite(&lVal, sizeof(lVal), 1, out);



	fread(&lVal, sizeof(lVal), 1, fp);
	fwrite(&lVal, sizeof(lVal), 1, out);

	fread(&lVal, sizeof(lVal), 1, fp);
	lVal = 18;
	fwrite(&lVal, sizeof(lVal), 1, out);

	fread(&sVal, sizeof(sVal), 1, fp);
	sVal = 7;
	fwrite(&sVal, sizeof(sVal), 1, out);

	fread(&sVal, sizeof(sVal), 1, fp);
	fwrite(&sVal, sizeof(sVal), 1, out);

	fread(&lVal, sizeof(lVal), 1, fp);
	fwrite(&lVal, sizeof(lVal), 1, out);

	fread(&lVal, sizeof(lVal), 1, fp);
	lVal = lVal >> 1;
	fwrite(&lVal, sizeof(lVal), 1, out);

	fread(&sVal, sizeof(sVal), 1, fp);
	sVal = sVal >> 1;
	fwrite(&sVal, sizeof(sVal), 1, out);

	fread(&sVal, sizeof(sVal), 1, fp);
	sVal = 8;
	fwrite(&sVal, sizeof(sVal), 1, out);

	sVal = 0;
	fwrite(&sVal, sizeof(sVal), 1, out);



	fread(&lVal, sizeof(lVal), 1, fp);
	fwrite(&lVal, sizeof(lVal), 1, out);

	fread(&lVal, sizeof(lVal), 1, fp);
	lVal = lVal >> 1;
	fwrite(&lVal, sizeof(lVal), 1, out);

	while(fread(&sample, sizeof(sample), 1, fp)) {
		asm volatile("mov %[samp], r1" : [samp] "=r" (sample));
		asm volatile("ulaw r1, r2");
		asm volatile("mov r2, %[out]" : [out] "=r" (comp));

		fwrite(&comp, sizeof(comp), 1, out);
	}

	fclose(fp);
	fclose(out);

	return 0;
}
