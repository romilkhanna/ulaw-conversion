library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ulaw_tb is
-- nothing
end entity;

architecture mu_compress of ulaw_tb is

signal data : std_logic_vector(15 downto 0);
signal clk : std_logic := '0';
signal dataq : std_logic_vector(7 downto 0);
constant clk_period : time := 2 ps;

component ulaw_compress is 
port( 
	data		:in		std_logic_vector(15 downto 0);	-- input
    clk         :in     std_logic;                      -- clock
    dataq       :out    std_logic_vector(7 downto 0));  -- output
end component; 

begin
	-- connect to component
	uut : ulaw_compress port map(data, clk, dataq);

	process
	begin
		clk <= '0';
		wait for clk_period / 2;
		clk <= '1';
		wait for clk_period / 2;
	end process;
    
	process
	begin
		-- test data
		data <= x"2801";
		wait for 210 ps;
		assert dataq = "00011011"
		  report "Failed to encode 2801 should be 00011011.";
		
		data <= x"BEFF";
		wait for 210 ps;
		assert dataq = "10010000"
		  report "Failed to encode BEFF should be 10010000.";
		
		data <= x"0D01";
		wait for 210 ps;
		assert dataq = "00110100"
		  report "Failed to encode 0D01 should be 00110100."; 
		
		data <= x"9C00";
		wait for 210 ps;
		assert dataq = "10100011"
		  report "Failed to encode 9C00 should be 10100011.";
		
		data <= x"86FD";
		wait for 210 ps;
		assert dataq = "11000001"
		  report "Failed to encode 86FD should be 11000001.";
			   
		wait;
    end process;
end mu_compress;
