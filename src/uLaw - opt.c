#include <stdio.h>

const int uLawBias = 33;
//const int uLawMaxClip = 0x7F7B;
const int uLawMaxClip = 0x1FDF;

static char uLawCompressTable[256] =
{
	0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,
	4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
	5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
	5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
	6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
	6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
	6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
	6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
	7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7
};

/*
 *	The 14-bit signed PCM value is encoded to 8-bit signed uLaw by:
 *		-	Storing the sign
 *		-	Clipping the magnitude at a max value of 2^13 - 1.
 *		-	Adding a bias to the original magnitude (33 in this case).
 *		-	The biased linear code has a leading 1 to indicate the segment number.
 *			The segment number = 7 - # of leading 0's.
 *		-	Trailing bits segment + 1 are discarded.
 *		-	The final value is complemented before returning.
 *
 *	uLaw example encoding table:	
 *
 *	Biased Linear Input Code	Compressed Code
 *	------------------------	---------------
 *		00000001wxyza			000wxyz
 *		0000001wxyzab			001wxyz
 *		000001wxyzabc			010wxyz
 *		00001wxyzabcd			011wxyz
 *		0001wxyzabcde			100wxyz
 *		001wxyzabcdef			101wxyz
 *		01wxyzabcdefg			110wxyz
 *		1wxyzabcdefgh			111wxyz
 *
 */

unsigned char PCM2uLaw(short sample) {
	sample = sample >> 2;
	int sign = (sample >> 8) & 0x80;
	if (sign)
		sample = (short)-sample;
	if (sample > uLawMaxClip)
		sample = uLawMaxClip;
	sample = (short)(sample + uLawBias);
	int segment = (int)uLawCompressTable[(sample>>5) & 0xFF];
	unsigned char data = (sample >> (segment+1)) & 0x0F;
	unsigned char compressedByte = ~ (sign | (segment << 4) | data);

	return compressedByte;
}

int main () {
	signed short sample;
	char comp;

	FILE *fp;
	fp = fopen("sample.wav", "rb");

	FILE *out;
	out = fopen("output.wav", "wb+");

	long chunkID;
	long chunkSize;
	long format;

	long subChunk1ID;
	long subChunk1Size;
	short audioFormat;
	short numChannels;
	long sampleRate;
	long byteRate;
	short blockAlign;
	short bitsPerSample;
	short extraParam;

	long subChunk2ID;
	long subChunk2Size;

	//read existing data
	fread(&chunkID, sizeof(chunkID), 1, fp);
	fread(&chunkSize, sizeof(chunkSize), 1, fp);
	fread(&format, sizeof(format), 1, fp);

	fread(&subChunk1ID, sizeof(subChunk1ID), 1, fp);
	fread(&subChunk1Size, sizeof(subChunk1Size), 1, fp);
	fread(&audioFormat, sizeof(audioFormat), 1, fp);
	fread(&numChannels, sizeof(numChannels), 1, fp);
	fread(&sampleRate, sizeof(sampleRate), 1, fp);
	fread(&byteRate, sizeof(byteRate), 1, fp);
	fread(&blockAlign, sizeof(blockAlign), 1, fp);
	fread(&bitsPerSample, sizeof(bitsPerSample), 1, fp);
	//fread(&extraParam, sizeof(extraParam), 1, fp); //not in pcm wav

	fread(&subChunk2ID, sizeof(subChunk2ID), 1, fp);
	fread(&subChunk2Size, sizeof(subChunk2Size), 1, fp);


	//recalc and re-write data
	audioFormat = 7; //ulaw
	subChunk1Size = 18; //bigger for ulaw
	bitsPerSample = 8; // halving this
	byteRate = byteRate >> 1; // half as big as before
	blockAlign = numChannels;
	extraParam = 0;//empty but still written

	subChunk2Size = subChunk2Size >> 1;//half the size since half the bits/sample
	chunkSize = 38 + subChunk2Size;

	fwrite(&chunkID, sizeof(chunkID), 1, out);
	fwrite(&chunkSize, sizeof(chunkSize), 1, out);
	fwrite(&format, sizeof(format), 1, out);

	fwrite(&subChunk1ID, sizeof(subChunk1ID), 1, out);
	fwrite(&subChunk1Size, sizeof(subChunk1Size), 1, out);
	fwrite(&audioFormat, sizeof(audioFormat), 1, out);
	fwrite(&numChannels, sizeof(numChannels), 1, out);
	fwrite(&sampleRate, sizeof(sampleRate), 1, out);
	fwrite(&byteRate, sizeof(byteRate), 1, out);
	fwrite(&blockAlign, sizeof(blockAlign), 1, out);
	fwrite(&bitsPerSample, sizeof(bitsPerSample), 1, out);
	fwrite(&extraParam, sizeof(extraParam), 1, out);

	fwrite(&subChunk2ID, sizeof(subChunk2ID), 1, out);
	fwrite(&subChunk2Size, sizeof(subChunk2Size), 1, out);


	while(fread(&sample, sizeof(sample), 1, fp)) {
		comp = PCM2uLaw(sample);
		//printf ("(%+hi) = %hi\n", sample, comp);
		fwrite(&comp, sizeof(comp), 1, out);
	}

	fclose(fp);
	fclose(out);

	return 0;
}
