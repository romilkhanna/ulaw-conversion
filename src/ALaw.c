#include <stdio.h>

const int uLawBias = 0x84;
const int uLawMaxClip = 0x7F7B;

static char uLawCompressTable[256] =
{
     0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,
     4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
     5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
     5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
     6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
     6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
     6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
     6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
     7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7
};

unsigned char PCM2uLaw(short sample) {
	//calc sign and if negative assign proper value
	int sign = 0;
	if(sample < 0) {
		sign = 0x80;
		sample *= -1;
	}

	//add the bias
	sample = sample + uLawBias;

	//if we're clipping it, clip it!
	if(sample > uLawMaxClip) {
		sample = uLawMaxClip;
	}


	int exponent = (int)uLawCompressTable[(sample>>7) & 0xFF];
	int mantissa = (sample >> (exponent+3)) & 0x0F;
	int compressedByte = ~ (sign | (exponent << 4) | mantissa);

	return (unsigned char)compressedByte;
}

int main () {
	signed short sample;
	char comp;

	FILE *fp;
	fp = fopen("sample.wav", "rb");

	FILE *out;
	out = fopen("output.wav", "wb+");

	long chunkID;
	long chunkSize;
	long format;

	long subChunk1ID;
	long subChunk1Size;
	short audioFormat;
	short numChannels;
	long sampleRate;
	long byteRate;
	short blockAlign;
	short bitsPerSample;
	short extraParam;

	long subChunk2ID;
	long subChunk2Size;

	//read existing data
	fread(&chunkID, sizeof(chunkID), 1, fp);
	fread(&chunkSize, sizeof(chunkSize), 1, fp);
	fread(&format, sizeof(format), 1, fp);

	fread(&subChunk1ID, sizeof(subChunk1ID), 1, fp);
	fread(&subChunk1Size, sizeof(subChunk1Size), 1, fp);
	fread(&audioFormat, sizeof(audioFormat), 1, fp);
	fread(&numChannels, sizeof(numChannels), 1, fp);
	fread(&sampleRate, sizeof(sampleRate), 1, fp);
	fread(&byteRate, sizeof(byteRate), 1, fp);
	fread(&blockAlign, sizeof(blockAlign), 1, fp);
	fread(&bitsPerSample, sizeof(bitsPerSample), 1, fp);
	//fread(&extraParam, sizeof(extraParam), 1, fp); //not in pcm wav

	fread(&subChunk2ID, sizeof(subChunk2ID), 1, fp);
	fread(&subChunk2Size, sizeof(subChunk2Size), 1, fp);


	//recalc and re-write data
	audioFormat = 7; //ulaw
	subChunk1Size = 18; //bigger for ulaw
	bitsPerSample = 8; // halving this
	byteRate = byteRate >> 1; // half as big as before
	blockAlign = numChannels;
	extraParam = 0;//empty but still written

	subChunk2Size = subChunk2Size >> 1;//half the size since half the bits/sample
	chunkSize = 38 + subChunk2Size;

	fwrite(&chunkID, sizeof(chunkID), 1, out);
	fwrite(&chunkSize, sizeof(chunkSize), 1, out);
	fwrite(&format, sizeof(format), 1, out);

	fwrite(&subChunk1ID, sizeof(subChunk1ID), 1, out);
	fwrite(&subChunk1Size, sizeof(subChunk1Size), 1, out);
	fwrite(&audioFormat, sizeof(audioFormat), 1, out);
	fwrite(&numChannels, sizeof(numChannels), 1, out);
	fwrite(&sampleRate, sizeof(sampleRate), 1, out);
	fwrite(&byteRate, sizeof(byteRate), 1, out);
	fwrite(&blockAlign, sizeof(blockAlign), 1, out);
	fwrite(&bitsPerSample, sizeof(bitsPerSample), 1, out);
	fwrite(&extraParam, sizeof(extraParam), 1, out);

	fwrite(&subChunk2ID, sizeof(subChunk2ID), 1, out);
	fwrite(&subChunk2Size, sizeof(subChunk2Size), 1, out);


	while(fread(&sample, sizeof(sample), 1, fp)) {
		comp = PCM2uLaw(sample);
		//printf ("(%+hi) = %hi\n", sample, comp);
		fwrite(&comp, sizeof(comp), 1, out);
	}

	fclose(fp);
	fclose(out);

	return 0;
}
