library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all; 
 
entity ulaw_14_8 is 
		PORT( 
			data			:in			std_logic_vector(13 downto 0);	--pcm linear signal 
			frame			:in			std_logic;								--frame synchronous signal 
			dataq			:out			std_logic_vector(7 downto 0));	--output noliear signal 
end ulaw_14_8; 
 
architecture rtl of ulaw_14_8 is 
	constant		data_c		:std_logic_vector(12 downto 0) := "0000000100001"; 
	signal		data_in		:std_logic_vector(13 downto 0); 
	signal		data_sum		:std_logic_vector(12 downto 0); 
	signal		data_q		:std_logic_vector(6 downto 0); 
begin 
	data_sum <= data(12 downto 0) + data_c; 
 
	process(frame) 
	begin 
		if rising_edge(frame) then 
			data_in <= data(13) & data_sum; 
		end if; 
	end process; 
 
	process(data_in) 
	begin 
			if data_in(12) = '1' then 
				data_q <= "111" & data_in(11 downto 8); 
			elsif data_in(11) = '1' then 
				data_q <= "110" & data_in(10 downto 7); 
			elsif data_in(10) = '1' then 
				data_q <= "101" & data_in(9 downto 6); 
			elsif data_in(9) = '1' then 
				data_q <= "100" & data_in(8 downto 5); 
			elsif data_in(8) = '1' then 
				data_q <= "011" & data_in(7 downto 4); 
			elsif data_in(7) = '1' then 
				data_q <= "010" & data_in(6 downto 3); 
			elsif data_in(6) = '1' then 
				data_q <= "001" & data_in(5 downto 2); 
--			elsif data_in(5) = '1' then 
			else 
				data_q <= "000" & data_in(4 downto 1); 
			end if; 
	end process; 
		 
	process(frame) 
	begin 
		if frame'event and frame = '1' then 
			dataq(7) <= data_in(13); 
			dataq(6 downto 0) <= data_q; 
		end if; 
	end process; 
end rtl; 